FROM registry.gitlab.com/trst-oss/nodejs:latest@sha256:2e46b4c33bfee41850bdbc7937a92321cb449ede8ab408d78880530f1b30db7d

RUN apt-get install -y binutils gcc pkg-config curl && rm -fr /var/cache/apt/archives/* /var/cache/apt/*.bin /var/log/* /var/cache/ldconfig/aux-cache

RUN curl -o rustup-init https://static.rust-lang.org/rustup/dist/x86_64-unknown-linux-gnu/rustup-init && chmod +x rustup-init && ./rustup-init --no-modify-path -y --profile minimal -c clippy -c rustfmt && rm rustup-init

ENV PATH="$PATH:/root/.cargo/bin"
